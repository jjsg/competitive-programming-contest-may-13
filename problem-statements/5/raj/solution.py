
def differenceFromStart(p,q,r,V,months):
	daysCount=0
	daysCount = daysCount + V*(r-0)
	while q>1:
		daysCount = daysCount + months[q-1]
		q = q-1
	daysCount = daysCount + (p-1)	
	return daysCount

tc=int(raw_input())

while tc:
	l=raw_input()
	arr = l.split()
	V=int(arr[0])
	Y=int(arr[1])
	end=Y+2
	months = [int(x) for x in arr[2:end]]
	daysInWeek = int(arr[end])
	start = end + 1
	weeks = arr[start:(start+daysInWeek)]
	start = start+daysInWeek
	p,q,r = [int(x) for x in arr[start: (start+3)]]
	start = start + 3
	weekDay = arr[start]
	weekInd = weeks.index(weekDay)
	start = start+1
	d,m,y = [int(x) for x in arr[start:]]
	firstCount = differenceFromStart(p,q,r,V,months)
	secondCount = differenceFromStart(d,m,y,V,months)
	if firstCount < secondCount:
		diff = secondCount - firstCount
		print weeks[(weekInd+diff)%daysInWeek]
	else:
		diff = firstCount - secondCount
		print weeks[(weekInd-diff)%daysInWeek]
	tc = tc -1
