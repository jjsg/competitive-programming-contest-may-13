count=0

def rotate(l,count):
	return l[count:] + l[:count]

def calculateDist(arr):
	d = 0
	for i in range(len(arr)):
		d = d + abs(arr[i]-i)
	return d

def merge_sort(li):

    if len(li) < 2: return li 
    m = len(li) / 2 
    return merge(merge_sort(li[:m]), merge_sort(li[m:])) 

def merge(l, r):
    global count
    result = [] 
    i = j = 0 
    while i < len(l) and j < len(r): 
        if l[i] < r[j]: 
            result.append(l[i])
            i += 1 
        else: 
            result.append(r[j])
            count = count + (len(l) - i)
            j += 1
    result.extend(l[i:]) 
    result.extend(r[j:]) 
    return result

tc=int(raw_input())
while tc:
	count = 0
	l=raw_input()
	num = [int(x) for x in l.split()]
        k = num[0]
        num = num[1:]
	mind = calculateDist(num)
	minarr=num
	rotationSteps=0 
	for i in range(1,len(num)):
		temparr = rotate(num,i)
		tempd = calculateDist(temparr)
		if tempd < mind:
			rotationSteps = i
			mind=tempd
			minarr = temparr
	count = rotationSteps
	merge_sort(minarr)
	print count
	tc = tc -1
