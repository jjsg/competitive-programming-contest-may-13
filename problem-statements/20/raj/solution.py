def isPresent(word, wordList):
	if word in wordList:
		return True
	return False

def findWordsUtil(mat, visitedList, x, y, s, words):
	visitedList[x][y]=1
	s = s + mat[x][y]
	if (isPresent(s.lower(), words)):
		possibleWords.append(s.lower())
	for u in range(x-1,x+2):
		for v in range(y-1, y+2):
			if (u == x or v ==y) and u>=0 and v>=0 and u<4 and v<4:
				if visitedList[u][v]==0:
				 	findWordsUtil(mat, visitedList, u,v,s,words)

	s = s[:-1]
	visitedList[x][y]=0

def findWords(mat,words):
	visitedList = [[0]*4,[0]*4,[0]*4,[0]*4]
	s=""
	for i in range(4):
		for j in range(4):
			findWordsUtil(mat, visitedList, i,j,s,words)

possibleWords = []
tc = int(raw_input())
while tc:
	l=raw_input()
	arr=l.split()
	x=int(arr[0])
	words = [x.lower() for x in arr[1:]]
	print words
	mat=[]
	mat.append(raw_input().split())
	mat.append(raw_input().split())
	mat.append(raw_input().split())
	mat.append(raw_input().split())
	findWords(mat,words)
	print possibleWords.sort()
	tc = tc - 1
"""
1
5 int num float string double
D O U B
N U M L
I O F E
N T L G
"""
